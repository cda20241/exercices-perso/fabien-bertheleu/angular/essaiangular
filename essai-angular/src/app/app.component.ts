import { Component,Input, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { AppareilComponent } from './appareil/appareil.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { DatePipe } from '@angular/common';
import { AppareilService } from './services/appareil.service';



@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, 
            CommonModule,
            FormsModule,
            DatePipe,
            AppareilComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})


  export class AppComponent implements OnInit{
    title = 'essai-angular';
    isAuth = false;

    lastUpdate = new Date()
   
    appareils = this.appareilService.appareils;

    constructor(private appareilService: AppareilService){ {
      setTimeout(
        () => {
          this.isAuth=true;
        }, 5000);
          }
        }

    ngOnInit()  {
        this.appareils = this.appareilService.appareils;
  }

  onAllumer() {
  console.log('On allume tout');
  }

  }