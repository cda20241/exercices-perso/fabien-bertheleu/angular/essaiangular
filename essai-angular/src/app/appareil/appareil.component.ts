import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-appareil',
  standalone: true,
  imports: [ReactiveFormsModule, 
            CommonModule, 
            FormsModule],
  templateUrl: './appareil.component.html',
  styleUrl: './appareil.component.scss'
})
export class AppareilComponent  {

  @Input() appareilName : string;
  @Input() appareilStatus : string;
  

  constructor() {
      }

  getStatus() {
    return this.appareilStatus;
  }

  getColor() {
    if (this.appareilStatus === 'allumé') {
      return "green";
    }
    else  {
        return "red";
      }
      
    }
  }
  

